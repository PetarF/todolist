<?php

// src/EventListener/UserChangedNotifier.php
namespace App\EventListener;

use App\Entity\Task;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Messenger\MessageBusInterface;
use App\Message\EmailTaskCompletedNotification;

class TaskCompletedDispatcher
{
    const STATUS = 'completed';
    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * TaskCompletedDispatcher constructor.
     * @param MessageBusInterface $bus
     */
    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }

    public function postUpdate(Task $task, LifecycleEventArgs $event)
    {
        if($task->getStatus()->getSystemName() == self::STATUS){
            $this->bus->dispatch(new EmailTaskCompletedNotification('Task '.$task->getName().' has completed.'));
        }

    }
}