<?php

namespace App\Service\ServiceInterface;

use App\Entity\TaskStatus;

interface TaskWorkflowServiceInterface
{
    /**
     * Checks for the next status available
     * @param TaskStatus $taskStatus
     * @param string $userRole
     * @return TaskStatus|null
     */
    public function getAvailableStatus(TaskStatus $taskStatus, string $userRole): ?TaskStatus;

    public function isStatusGranted(TaskStatus $taskStatus): bool;
}