<?php
declare(strict_types=1);

namespace App\Service;


use App\Entity\TaskStatus;
use App\Entity\TaskWorkflow;
use App\Repository\TaskWorkflowRepository;
use App\Service\ServiceInterface\TaskWorkflowServiceInterface;
use Symfony\Component\Security\Core\Security;

class TaskWorkflowService implements TaskWorkflowServiceInterface
{
    /**
     * @var TaskWorkflowRepository
     */
    private $taskWorkflowRepository;
    /**
     * @var Security
     */
    private $security;

    /**
     * TaskWorkflowService constructor.
     * @param TaskWorkflowRepository $taskWorkflowRepository
     * @param Security $security
     */
    public function __construct(TaskWorkflowRepository $taskWorkflowRepository, Security $security)
    {
        $this->taskWorkflowRepository = $taskWorkflowRepository;
        $this->security = $security;
    }


    /**
     * @inheritDoc
     */
    public function getAvailableStatus(TaskStatus $taskStatus, string $userRole): ?TaskStatus
    {
        /**
         * @var TaskWorkflow|null $taskWorkflow
         */
        $taskWorkflow = $this->taskWorkflowRepository->findByCurrentStatusAndRole($taskStatus, $userRole);

        return !empty($taskWorkflow) ? $taskWorkflow->getAvailableStatus() : null;

    }


    public function isStatusGranted(TaskStatus $taskStatus): bool
    {
        //Pass status check to security componenet
        if ($this->security->isGranted($taskStatus->getSystemName())) {
            return true;
        }

        return false;
    }
}