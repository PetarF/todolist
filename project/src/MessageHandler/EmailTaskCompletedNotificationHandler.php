<?php 
namespace App\MessageHandler;

use App\Message\EmailTaskCompletedNotification;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

use Symfony\Component\Mailer\MailerInterface;

use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;

class EmailTaskCompletedNotificationHandler implements MessageHandlerInterface
{
    /**
     * @var MailerInterface
     */
    private $mailer;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * EmailTaskCompletedNotificationHandler constructor.
     * @param MailerInterface $mailer
     * @param UserRepository $userRepository
     */
    public function __construct(MailerInterface $mailer, UserRepository $userRepository)
    {
        $this->mailer = $mailer;
        $this->userRepository = $userRepository;
    }


    public function __invoke(EmailTaskCompletedNotification $message)
    {
        $subject = 'Task has been completed';
        $body = $message->getContent();
        $users = $this->userRepository->findByRole('ROLE_ADMIN');

        if(empty($users)){
            throw new \RuntimeException('No users');
        }
        $toAddressArray = [];
        foreach ($users as $user){
            $toAddressArray[] = new Address($user->getEmail(),$user->getFullName());
        }

        $email = (new Email())
            ->from(new Address('app@test.com', 'Todo App'))
            ->to(...$toAddressArray)
            ->subject($subject)
            ->html($body);

        $this->mailer->send($email);
    }
}