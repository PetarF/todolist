<?php

namespace App\Repository;

use App\Entity\TaskWorkflow;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TaskWorkflow|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaskWorkflow|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaskWorkflow[]    findAll()
 * @method TaskWorkflow[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskWorkflowRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaskWorkflow::class);
    }

    public function findByCurrentStatusAndRole($currentStatus,$role)
    {
        $qb = $this->createQueryBuilder('x');
        $qb->where('x.currentStatus = :currentStatus')
            ->setParameter('currentStatus',$currentStatus)
            ->andWhere('x.role LIKE :roles')
            ->setParameter('roles', '%'.$role.'%')
        ;

        return $qb->getQuery()->getOneOrNullResult();

    }

    // /**
    //  * @return TaskWorkflow[] Returns an array of TaskWorkflow objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TaskWorkflow
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
