<?php
declare(strict_types=1);

namespace App\Security;


use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

use Symfony\Component\Security\Core\Security;

class UserTaskVoter extends Voter
{
   

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports( $attribute, $subject)
    {

        if (!$attribute instanceof User) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute( $attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        
        
        if(in_array('ROLE_ADMIN', $user->getRoles())){
            return true;
        }

        if($user == $attribute){
            return true;
        }

        
        return false;
    }
}