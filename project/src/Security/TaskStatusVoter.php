<?php
declare(strict_types=1);

namespace App\Security;


use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

use Symfony\Component\Security\Core\Security;

class TaskStatusVoter extends Voter
{
    const CANCELLED = 'cancelled';
    const CLOSED = 'closed';

    protected function supports(string $attribute, $subject)
    {

        if (!in_array($attribute, [self::CANCELLED, self::CLOSED])) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if(in_array('ROLE_ADMIN', $user->getRoles())){
            return true;
        }

        return false;
    }
}