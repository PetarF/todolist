<?php

namespace App\Form;

use App\Entity\Task;
use App\Entity\TaskStatus;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class TaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description');
        if($options['new'] !== true){
            if($options['edit'] !== true){
                $builder->add('status', EntityType::class,['class'=>TaskStatus::class,'choice_label' => 'name',]);
            }else{
                $builder->add('status', EntityType::class,['class'=>TaskStatus::class,'choice_label' => 'name','disabled'=>true]);
            }
        }else{
            //$builder->add('status', HiddenType::class,['data' => 1,]);
            $builder->add('status', EntityType::class,['class'=>TaskStatus::class,'choice_label' => 'name','disabled'=>true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('x')
                        ->where("x.systemName =:name ")->setParameter(':name', 'pending');
                },]);
        }

        $builder->add('user',EntityType::class,['class'=>User::class,'choice_label' => 'fullname',]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
            'new'=>null,
            'edit'=>null
        ]);
    }
}
