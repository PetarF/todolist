<?php

namespace App\Form;

use App\Entity\TaskWorkflow;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Entity\TaskStatus;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Form\DataTransformer\RolesTransformer;

class TaskWorkflowType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            
            ->add('currentStatus', EntityType::class,['class'=>TaskStatus::class,'choice_label' => 'name',])
            ->add('availableStatus', EntityType::class,['class'=>TaskStatus::class,'choice_label' => 'name',])
            ->add('role', ChoiceType::class, array(
                'choices' => array(
                    'User' => 'ROLE_USER',
                    'Admin' => 'ROLE_ADMIN'
                ),

            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TaskWorkflow::class,
        ]);
    }
}
