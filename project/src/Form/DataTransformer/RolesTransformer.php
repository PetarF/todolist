<?php

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class RolesTransformer implements DataTransformerInterface
{
    public function transform($roles)
    {
        foreach ($roles as $role) {

            if ($role !== 'ROLE_USER') {
                return $role;
            }
        }

        return 'ROLE_USER';
    }

    public function reverseTransform($string)
    {
        if(empty($string)){
            $string = 'ROLE_USER';
        }
        return [
            $string
        ];
    }
}