<?php

namespace App\Form\DataTransformer;


use Symfony\Component\Form\DataTransformerInterface;

class StringToArrayTransformer implements DataTransformerInterface
{
    public function transform($data)
    {

        if(!is_array($data)){
            $data = explode(',',$data);
        }
        
        return $data;

    }

    public function reverseTransform($string)
    {
       
        if(is_array($string)){
            $data =  implode(',',$string);
        }else{
            $data = $string;
        }
        
        return $data;
    }
}