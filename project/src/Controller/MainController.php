<?php

namespace App\Controller;

use App\Entity\Task;
use App\Form\TaskType;
use App\Repository\TaskRepository;
use App\Repository\TaskStatusRepository;
use App\Service\ServiceInterface\TaskWorkflowServiceInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class MainController extends AbstractController
{

    const IN_PROGRESS = 'in-progress';

    /**
     * @var TaskStatusRepository
     */
    private $taskStatusRepository;
    /**
     * @var TaskRepository
     */
    private $taskRepository;
    /**
     * @var TaskWorkflowServiceInterface
     */
    private $taskWorkflowService;

    /**
     * MainController constructor.
     * @param TaskStatusRepository $taskStatusRepository
     * @param TaskRepository $taskRepository
     * @param TaskWorkflowServiceInterface $taskWorkflowService
     */
    public function __construct(TaskStatusRepository $taskStatusRepository, TaskRepository $taskRepository, TaskWorkflowServiceInterface $taskWorkflowService)
    {
        $this->taskStatusRepository = $taskStatusRepository;
        $this->taskRepository = $taskRepository;
        $this->taskWorkflowService = $taskWorkflowService;
    }


    /**
     * @Route("/", name="main_index")
     */
    public function index()
    {
        $user = $this->getUser();

        $taskStatuses = $this->taskStatusRepository->findBy(['is_active' => true]);
        $data = [];
        if ($taskStatuses) {

            foreach ($taskStatuses as $status) {

                if (!$this->isGranted('ROLE_VIEW_ALL_TASKS')) {
                    $tasks = $this->taskRepository->findBy(['user' => $user, 'status' => $status]);
                } else {
                    $tasks = $status->getTasks();
                }
                $data[] = ['status' => $status, 'tasks' => $tasks];
            }
        }

        return $this->render('main/index.html.twig', ['user' => $user, 'data' => $data]);
    }

    /**
     * @Route("/handle/task/{id}", name="task_handle", methods={"GET","POST"}, defaults={"id": null})
     * @IsGranted("ROLE_ADMIN")
     * 
     * @param Request $request
     * @param Task|null $task
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function handleTask(Request $request, Task $task = null)
    {

        $new = null;
        $edit = null;
        if (!empty($task)) {
            $edit = true;
        } else {
            $task = new Task();
            $new = true;
        }

        $form = $this->createForm(TaskType::class, $task, ['new' => $new, 'edit' => $edit]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($task);
            $entityManager->flush();

            return $this->redirectToRoute('main_index');
        }

        return $this->render('task/edit.html.twig', [
            'task' => $task,
            'form' => $form->createView(),
            'backToList' => false,
        ]);
    }

    /**
     * @Route("/handle/status/{id}/{systemStatusName}", name="task_handle_status", methods={"GET","POST"}, defaults={"systemStatusName": null})
     * @param Task $task
     * @param null $systemStatusName
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function handleTaskStatus(Task $task, $systemStatusName = null)
    {

        if(!$this->isGranted($task->getUser())){
            $this->addFlash('notice', 'You can only work with your tasks!');
            return $this->redirectToRoute('main_index');
        }
        
        $changedUser = false;
        $roles = $task->getUser()->getRoles();
        $taskStatus = $task->getStatus();

        //check if logged in user is the owner of the task
        if ($this->getUser() != $task->getUser()) {

            $roles = $this->getUser()->getRoles();

            $changedUser = true;
        }

            $availableStatus = null;

        foreach ($roles as $role) {

            if(empty($availableStatus)){
                $availableStatus = $this->taskWorkflowService->getAvailableStatus($taskStatus, $role);        
            }

            if($role == 'ROLE_ADMIN'){
                $availableStatus = $this->taskWorkflowService->getAvailableStatus($taskStatus, $role);           
            }

        }

        if (!empty($availableStatus)) {
            $task->setStatus($availableStatus);
        }

        if (empty($systemStatusName)) {
            if (empty($availableStatus)) {
                $this->addFlash('notice', 'There was a problem processing your request!');
            }
        } else {

            $status = $this->taskStatusRepository->findOneBy(['systemName' => $systemStatusName]);

            if (null != $status) {
                if ($this->taskWorkflowService->isStatusGranted($status)) {
                    $task->setStatus($status);
                    $changedUser = false;
                }else{
                    $this->addFlash('notice', 'Change in status ' . $systemStatusName . ' is not allowed!');
                }
            } else {
                $this->addFlash('notice', 'Status ' . $systemStatusName . ' does not exist!');
            }

        }

        if($task->getStatus()->getSystemName() == self::IN_PROGRESS){
            $tasksInProgress = $this->taskRepository->findOneBy(['status'=>$task->getStatus(), 'user'=>$this->getUser()]);
            if(!empty($tasksInProgress)){
                $this->addFlash('notice', 'You can only have one Task IN PROGRESS');
                return $this->redirectToRoute('main_index');
            }
        }


        //change task owner
        if ($changedUser) {
            $task->setUser($this->getUser());
        }

        if ($taskStatus != $task->getStatus()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($task);
            $entityManager->flush();
            $this->addFlash('notice', 'Your changes were saved!');
        }
        return $this->redirectToRoute('main_index');
    }

}