<?php

namespace App\Controller;

use App\Entity\TaskWorkflow;
use App\Form\TaskWorkflowType;
use App\Repository\TaskWorkflowRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/task-workflow")
 * @IsGranted("ROLE_ADMIN")
 */
class TaskWorkflowController extends AbstractController
{
    /**
     * @Route("/", name="task_workflow_index", methods={"GET"})
     */
    public function index(TaskWorkflowRepository $taskWorkflowRepository): Response
    {
        return $this->render('task_workflow/index.html.twig', [
            'task_workflows' => $taskWorkflowRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="task_workflow_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $taskWorkflow = new TaskWorkflow();
        $form = $this->createForm(TaskWorkflowType::class, $taskWorkflow);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($taskWorkflow);
            $entityManager->flush();

            return $this->redirectToRoute('task_workflow_index');
        }

        return $this->render('task_workflow/new.html.twig', [
            'task_workflow' => $taskWorkflow,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="task_workflow_show", methods={"GET"})
     */
    public function show(TaskWorkflow $taskWorkflow): Response
    {
        return $this->render('task_workflow/show.html.twig', [
            'task_workflow' => $taskWorkflow,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="task_workflow_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TaskWorkflow $taskWorkflow): Response
    {
        $form = $this->createForm(TaskWorkflowType::class, $taskWorkflow);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('task_workflow_index');
        }

        return $this->render('task_workflow/edit.html.twig', [
            'task_workflow' => $taskWorkflow,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="task_workflow_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TaskWorkflow $taskWorkflow): Response
    {
        if ($this->isCsrfTokenValid('delete'.$taskWorkflow->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($taskWorkflow);
            $entityManager->flush();
        }

        return $this->redirectToRoute('task_workflow_index');
    }
}
