<?php

namespace App\DataFixtures;

use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Entity\User;

use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;

class UserFixtures extends Fixture implements FixtureGroupInterface
{

    public static function getGroups(): array
    {
        return ['User','Task'];
    }

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $users = [
            ['admin', 'admin', ['ROLE_ADMIN'], 'Admin', 'admin@test.com'],
            ['user', 'user', ['ROLE_USER'], 'User', 'user@test.com'],
            ['user2', 'user2', ['ROLE_USER'], 'User2', 'user2@test.com'],
        ];

        foreach ($users as $userData) {
            $user = new User();

            $user->setUsername($userData[0]);
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                $userData[1]
            ));
            $user->setRoles($userData[2]);
            $user->setFullName($userData[3]);
            $user->setEmail($userData[4]);
            $manager->persist($user);
        }

        $manager->flush();
    }
}