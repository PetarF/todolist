<?php

namespace App\DataFixtures;

use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Entity\TaskWorkflow;

use App\Repository\TaskStatusRepository;

use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class TaskWorkflowFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    
    /**
     * @var TaskStatusRepository
     */
    private $taskStatusRepository;

    /**
     * TaskWorkflowFixtures constructor.
     * @param TaskStatusRepository $taskStatusRepository
     */
    public function __construct( TaskStatusRepository $taskStatusRepository)
    {
        $this->taskStatusRepository = $taskStatusRepository;
    }


    public function getDependencies()
    {
        return [
            TaskStatusFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return ['TaskWorkflow',];
    }

    public function load(ObjectManager $manager)
    {
        
        $pending = $this->taskStatusRepository->findOneBy(['systemName'=>'pending']);
        $completed = $this->taskStatusRepository->findOneBy(['systemName'=>'completed']);
        $inProgress = $this->taskStatusRepository->findOneBy(['systemName'=>'in-progress']);
        

        $dataArray = [
            [$pending,$inProgress,'ROLE_USER'],
            [$inProgress,$completed,'ROLE_USER'],

            [$pending,$inProgress,'ROLE_ADMIN'],
            [$inProgress,$completed,'ROLE_ADMIN'],


        ];

        foreach ($dataArray as $data) {
            $taskWorkflow = new TaskWorkflow();

            $taskWorkflow->setCurrentStatus($data[0]);
            $taskWorkflow->setAvailableStatus($data[1]);
            $taskWorkflow->setRole($data[2]);
           
            $manager->persist($taskWorkflow);
        }

        $manager->flush();
    }
}