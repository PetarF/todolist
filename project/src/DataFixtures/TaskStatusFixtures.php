<?php

namespace App\DataFixtures;

use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Entity\TaskStatus;

use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;

class TaskStatusFixtures extends Fixture implements FixtureGroupInterface
{
    
    public static function getGroups(): array
     {
         return ['TaskStatus','Task'];
     }

    public function load(ObjectManager $manager)
    {
        $dataArray = [
            ['PENDING',1],
            ['IN PROGRESS',2],
            ['COMPLETED',3],
            ['CANCELLED',4],
            ['CLOSED',5],
        ];

        foreach ($dataArray as $data) {
            $taskStatus = new TaskStatus();

            $taskStatus->setName($data[0]);
            $taskStatus->setPriority($data[1]);
            $taskStatus->setIsActive(true);
           
            $manager->persist($taskStatus);
        }

        $manager->flush();
    }
}