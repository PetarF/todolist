<?php

namespace App\DataFixtures;

use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Entity\Task;

use App\Repository\TaskStatusRepository;
use App\Repository\UserRepository;

use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class TaskFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var TaskStatusRepository
     */
    private $taskStatusRepository;

    /**
     * TaskFixtures constructor.
     * @param UserRepository $userRepository
     * @param TaskStatusRepository $taskStatusRepository
     */
    public function __construct(UserRepository $userRepository, TaskStatusRepository $taskStatusRepository)
    {
        $this->userRepository = $userRepository;
        $this->taskStatusRepository = $taskStatusRepository;
    }


    public function getDependencies()
    {
        return [
            UserFixtures::class,
            TaskStatusFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return ['Task',];
    }

    public function load(ObjectManager $manager)
    {
        $taskStatusArray = $this->taskStatusRepository->findBy(['systemName'=>['pending','completed','cancelled','closed']]);
        $userArray = $this->userRepository->findBy(['id'=>[2,3]]);

        for ($i = 0; $i < 10; $i++) {
            $task = new Task();
            $task->setName('Task '.$i);
            $task->setDescription('Description for Task '.$i);
            $task->setUser($userArray[array_rand($userArray,1)]);
            $task->setStatus($taskStatusArray[array_rand($taskStatusArray,1)]);
            $manager->persist($task);
        }

        $manager->flush();
    }
}