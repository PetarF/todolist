<?php
namespace App\Message;

use App\Message\MessageInterface\AsyncMessageInterface;

class EmailTaskCompletedNotification implements AsyncMessageInterface
{
    private $content;

    public function __construct(string $content)
    {
        $this->content = $content;
    }

    public function getContent(): string
    {
        return $this->content;
    }

}
