<?php

namespace App\Entity;

use App\Repository\TaskWorkflowRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Table(name="task_workflow",
 *    uniqueConstraints={
 *        @UniqueConstraint(name="task_workflow_unique",
 *            columns={"current_status_id", "available_status_id", "role"})
 *    }
 * )
 * @ORM\Entity(repositoryClass=TaskWorkflowRepository::class)
 */
class TaskWorkflow
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=TaskStatus::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $currentStatus;

    /**
     * @ORM\ManyToOne(targetEntity=TaskStatus::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $availableStatus;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $role;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCurrentStatus(): ?TaskStatus
    {
        return $this->currentStatus;
    }

    public function setCurrentStatus(?TaskStatus $currentStatus): self
    {
        $this->currentStatus = $currentStatus;

        return $this;
    }

    public function getAvailableStatus(): ?TaskStatus
    {
        return $this->availableStatus;
    }

    public function setAvailableStatus(?TaskStatus $availableStatus): self
    {
        $this->availableStatus = $availableStatus;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role): void
    {
        $this->role = $role;
    }


}
