@echo off

if "%~1"=="" (
 echo "EXECUTING: docker-compose -f docker-compose.yml up --build -d"
 docker-compose -f docker-compose.yml up --build -d 
 exit /b 0
)
If %1==-h (
 echo "without arguments runs docker-compose -f docker-compose.yml up --build -d"
 echo "front - runs docker-compose up --build -d"
 echo "stop - stops active containers"
 echo "down - stops and removes active containers"
 echo "service [name-of-service] - enters active service in shell"
 echo "log [name-of-service] - displays logs of service in front"
 exit /b 0
)
If %1==front (
 echo "EXECUTING: docker-compose up --build -d"
 docker-compose up --build -d
 exit /b 0
)
if %1==stop (
 echo "EXECUTING: docker-compose stop"
 docker-compose stop
 exit /b 0
)
If %1==down (
 echo "EXECUTING: docker-compose down"
 docker-compose down
 exit /b 0
)
If %1==service (
  	if "%~2"=="" (
 	 echo "you must provide service name"
 	 exit /b 0
 	) else (
 		echo "EXECUTING: docker-compose exec %2 sh"
  		docker-compose exec %2 sh
  		exit /b 0
 	)
  exit /b 0
)
If %1==log (
  	if "%~2"=="" (
 	 echo "EXECUTING: docker-compose logs -f"
 	 docker-compose logs -f
 	 exit /b 0
 	) else (
 		echo "EXECUTING: docker-compose logs -f %2"
  		docker-compose logs -f %2 
  		exit /b 0
 	)
  exit /b 0
)