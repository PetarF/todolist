#!/usr/bin/env sh
cd /opt/project/;
chown -R www-data:www-data /opt/project && find /opt/project -type d -exec chmod 755 {} \; && find /opt/project -type f -exec chmod 644 {} \;

FILE=./node_modules/.yarn-integrity
if [ -f "$FILE" ]; then
    yarn encore dev --watch;
else
    yarn install;
	yarn encore dev --watch;
fi