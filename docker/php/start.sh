#!/usr/bin/env sh

if [ ! -f /opt/chmod.txt ] 
then
	touch /opt/chmod.txt
chown -R www-data:www-data /opt/project
find /opt/project -type d -exec chmod 755 {} \;
find /opt/project -type f -exec chmod 644 {} \;
fi 

/usr/local/sbin/php-fpm