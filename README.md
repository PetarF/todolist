Todo List

UI:

Mailhog@11025

PhpMyAdmin@13306 admin:admin

Supervisord@19001  supervisor:supervisor

Users:

Admin   admin@test.com  admin:admin

User    user@test.com   user:user

User2   user2@test.com  user2:user2


How to start the project:

Start docker container:
docker-compose up --build -d

Enter the php container:
docker-compose exec php sh

Install app:
composer install

Bring up the database:
php bin/console doctrine:migrations:migrate

Install fixtures:
php bin/console doctrine:fixtures:load


Application is mapped to port 80 so go to your favorite browser and in the address bar type in localhost. Login screen should open.